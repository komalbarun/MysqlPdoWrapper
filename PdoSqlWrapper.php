<?php

declare(strict_types=1);

namespace Komalbarun;


class PdoSqlWrapper
{
	public function __construct(array $config)
	{
		$host = $config['host'];
		$dbname = $config['dbname'];
		$user = $config['user'];
		$pass = $config['password'];
		$charset = $config['charset'];
		$port = empty($config['port']) ? '3306': $config['port'];
		$dbtype = $config['dbtype'];

		$dsn = "{$dbtype}:host={$host};port={$port};";
		$dsn .= "dbname={$dbname};charset={$charset}";


		$options = [
			\PDO::ATTR_ERRMODE  => \PDO::ERRMODE_EXCEPTION,
    		\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
    		\PDO::ATTR_EMULATE_PREPARES => false,
		];

		$this->db = new \PDO($dsn, $user, $pass, $options);
	}

	/**
	 * PDO::fetch() looped
	 * Use this when prepared statements are not required,
	 * like when query has no user input.
	 * 
	 * @param  string $sql   
	 */
	public function fetch_simple_sql(string $sql): \generator
	{
		$results = $this->db->query($sql);

		while ( $result = $results->fetch()) 
		{
			yield $result;
		}

	}

	/**
	 * PDO::fetch()
	 * Use this when prepared statements are not required,
	 * like when query has no user input.
	 * 
	 * @param  string $sql
	 * @return  array
	 */
	public function fetch_simple_sql_all(string $sql): array
	{
		$results = $this->db->query($sql);
		return $results->fetchAll();
	}

	/**
	 * PDO::fetch() looped
	 * Used when sql contains user input.
	 * 
	 * @param  string $sql 
	 * @param  array  $data data to replace '?' in sqls with.
	 * @return generator 
	 */
	public function fetch_prepared_sql(string $sql, array $data): \generator
	{
		$prepared = $this->db->prepare($sql);
		$prepared->execute($data);

		while ( $result = $prepared->fetch())
		{
			yield $result;
		}
	}

	/**
	 * PDO::fetch()
	 * Used when sql contains user input.
	 * 
	 * @param  string $sql 
	 * @param  array  $data data to replace '?' in sqls with.
	 * @return array 
	 */
	public function fetch_prepared_sql_all(string $sql, array $data): array
	{
		$prepared = $this->db->prepare($sql);
		$prepared->execute($data);
		return $prepared->fetchAll();
	}

	/**
	 * Executes sql for CRUD with no user inputs.
	 * 
	 * @param  string $sql
	 */
	public function simple_execute_sql(string $sql)
	{
		$stmt = $this->db->query($sql);
		$stmt->execute();
	}

	/**
	 * Executes sql for CRUD with user inputs.
	 * 
	 * @param  string $sql
	 * @param  array  $data 
	 */
	public function prepared_execute_sql(string $sql, array $data)
	{
		$prepared = $this->db->prepare($sql);
		$prepared->execute($data);
	}

	/**
	 * Fetches a single row from database (user input).
	 * 
	 * @param  string $sql
	 * @param  array  $data
	 */
	public function fetch_prepared_one(string $sql, array $data)
	{
		$prepared =  $this->db->prepare($sql);
		$prepared->execute($data);
	
		return $prepared->fetch();
	}

	/**
	 * Fetches a single row from database (no user input).
	 * 
	 * @param  string $sql
	 */
	public function fetch_simple_one(string $sql)
	{
		$prepared =  $this->db->prepare($sql);
		$prepared->execute();
	
		return $prepared->fetch();
	}
}