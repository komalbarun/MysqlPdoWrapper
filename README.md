## PdoSqlWrapper
--------
 Simple wrapper for mysql with PDO

## Installation
----------------
 composer require komalbarun\pdosqlwrapper

## Dependencies
----------------
 * php >= 7.0
 
## Usage
---------
```php
<?php

require_once './vendor/autoload.php';

use Komalbarun\PdoSqlWrapper;

//config assoc
$config = [
    'host' => '',
    'dbname' => '',
    'user' => '',
    'password' => '',
    'charset' => ''
    'port' => ''
];

$db = new PdoSqlWrapper($config);

// no user input fetch
// $results is a generator object.
$results = $db->fetch_simple_sql($sql);

// user input fetch (uses prepared statement).
// $results is a generator object.
// $data is an array 
$results = $db->fetch_prepared_sql($sql, $data);

// no user input CUD
$db->simple_execute_sql($sql);

// User input CUD (uses prepared statement)
// // $data is an array 
$db->prepared_execute_sql($sql, $data)

//PDO::fetchAll()
$db->fetch_simple_sql_all($sql)

//PDO::fetchAll()
$db->fetch_prepared_sql_all($sql, $data)
```
